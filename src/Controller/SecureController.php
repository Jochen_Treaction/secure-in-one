<?php
// COMMENT: read README.md to setup the microservice properly

namespace App\Controller;


use App\Services\CryptService;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/secure")
 * Class SecureController
 * @package App\Controller
 * @internal read README.md
 */
class SecureController extends AbstractController
{

    /**
     * @Route("/encstring", name="encString", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function encryptString(Request $request, CryptService $cryptionService, LoggerInterface $logger): JsonResponse
    {
        $data = $request->getContent();
        $arr = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $string = $cryptionService->encrypt($arr['string']);
        return $this->json(['string' => $string]);
    }


    /**
     * @Route("/decstring", name="decString", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function decryptString(Request $request, CryptService $cryptionService): JsonResponse
    {
        $data = $request->getContent();
        $arr = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $string = $cryptionService->decrypt($arr['string']);
        return $this->json(['string' => $string]);
    }


    /**
     * @Route("/encrecord", name="encRecord", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function encryptRecord(Request $request, CryptService $cryptionService, LoggerInterface $logger): JsonResponse
    {
        $data = $request->getContent();
        $record = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $return = $cryptionService->encryptRecord($record);

        return $this->json($return);
    }


    /**
     * @Route("/decrecord", name="decRecord", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function decryptRecord(Request $request, CryptService $cryptionService, LoggerInterface  $logger): JsonResponse
    {
        $data = $request->getContent();
        $record = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $return = $cryptionService->decryptRecord($record);

        return $this->json($return);
    }


    /**
     * @Route("/encrecordset", name="encRecordSet", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function encryptRecordSet(Request $request, CryptService $cryptionService, LoggerInterface $logger): JsonResponse
    {
        $data = $request->getContent();
        $recordSet = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $return = $cryptionService->encryptRecordSet($recordSet);
        return $this->json($return);
    }


    /**
     * @Route("/decrecordset", name="decRecordSet", methods={"POST"})
     * @param Request      $request
     * @param CryptService $cryptionService
     * @return JsonResponse
     */
    public function decryptRecordSet(Request $request, CryptService $cryptionService): JsonResponse
    {
        $data = $request->getContent();
        $rescordSet = json_decode($data, true, 512, JSON_OBJECT_AS_ARRAY);
        $return = $cryptionService->decryptRecordSet($rescordSet);
        return $this->json($return);
    }

}
