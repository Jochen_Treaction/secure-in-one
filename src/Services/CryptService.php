<?php


namespace App\Services;

use Psr\Log\LoggerInterface;
use Exception;

/**
 * read README.md
 * Class EnDeCryptionService
 * @package App\Services
 */
class CryptService
{

    protected $log;
    private $key;

    /**
     * EnDeCryptionService constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        // todo: implement and get key as described in https://symfony.com/doc/current/configuration/secrets.html
        // get the from a secret place, do not define it here!!!!
        // $key MUST BE exactly 32 characters long!!!
        $this->key = md5('this is my not so long key h@J5gdP|p0,dZEiHw&uCzAQ5DkCQ<~Rh0&M;D9U)rDEFsUyS/*jeFy\Q;OZ}(UTGR)yS:%|-kv$^0,aczA;>p>z*k?.OciersBhfj7myZ)r2Uy.6-F!X9F@[rM4E<0P$');
        $this->log = $logger;

    }


    /**
     * @param string $string
     * @return string
     */
    public function encrypt(string $message, $key=null): string
    {
        $key = (null === $key) ? $this->key : $key;
        if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
            throw new RangeException('Key is not the correct size (must be 32 bytes).');
        }
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

        $cipher = base64_encode(

            $nonce .
            sodium_crypto_secretbox(
                $message,
                $nonce,
                $key
            )

        );
        sodium_memzero($message);
        sodium_memzero($key);
        return $cipher;
    }


    public function decrypt(string $encrypted, string $key = null): string
    {
        $key = (null === $key) ? $this->key : $key;
        $decoded = base64_decode($encrypted);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $plain = sodium_crypto_secretbox_open(
            $ciphertext,
            $nonce,
            $key
        );
        if (!is_string($plain)) {
            throw new Exception('Invalid MAC');
        }
        sodium_memzero($ciphertext);
        sodium_memzero($key);
        return $plain;
    }


    public function encryptRecord(array $record): array
    {
        foreach ($record as $k => $v) {
            $isNumber = (1 !== preg_match('/[^0-9\.,]+/', $v)); // leave out numbers
            $isDate = (1 === preg_match('/^(\d{4}.{1}\d{2}.{1}\d{2})/', $v)); // leave out dates

            if ($isNumber || $isDate) {
                $return[$k] = $v;
            } else {
                $return[$k] = $this->encrypt($v);
            }
        }
        return $return;
    }


    /**
     * @param array $record
     * @return array
     */
    public function decryptRecord(array $record): array
    {
        foreach ($record as $k => $v) {
            $isNumber = (1 !== preg_match('/[^0-9\.,]+/', $v)); // leave out numbers
            $isDate = (1 === preg_match('/^(\d{4}.{1}\d{2}.{1}\d{2})/', $v)); // leave out dates

            if ($isNumber || $isDate) {
                $return[$k] = $v;
            } else {
                $return[$k] = $this->decrypt($v);
            }
        }
        return $return;
    }


    /**
     * @param array $record
     * @return array
     */
    public function encryptRecordSet(array $record): array
    {
        foreach ($record as $k => $v) {
                 $return[$k] = $this->encryptRecord($v);
        }
        return $return;
    }


    /**
     * @param array $record
     * @return array
     */
    public function decryptRecordSet(array $record): array
    {
        foreach ($record as $k => $v) {
                $return[$k] = $this->decryptRecord($v);
        }
        return $return;
    }

}
