Name: SECURE
Version 2.0.3.0

# README - SECURE-IN-ONE MICROSERVICE #

### TODO (Status 2020-10-12) ###
* Store and access the KEY needed for en-/decryption by following https://symfony.com/doc/current/configuration/secrets.html
* restrict access to SECURE-IN-ONE microservice on IP level (server side configuration + optionally symfony based security on ip level)

### What is this repository for? ###

* Sodium based encryption and decryption of strings / records / recordsets
* Version 2 (Sodium)
* Author: jsr


### How do I get set up? ###
* for symfony secret key setup see https://symfony.com/doc/current/configuration/secrets.html    
* for a **fast service** on production you'll need to
* install **swoole php extension** to your local server, to dev-server, to prod-server 
* install https://www.swoole.co.uk/article/symfony-swoole
* Configuration composer.json to run secure-in-one on **swoole**:
`{
    "type": "project",
    "license": "proprietary",
    "require": {
        "php": ">=7.3.4",
        "ext-ctype": "*",
        "ext-json": "*",
        "ext-iconv": "*",
        "ext-swoole": " ^4.4.7",
        "doctrine/annotations": "^1.10",
        "k911/swoole-bundle": "^0.8.2",
        "symfony/console": "5.1.*",
        "symfony/dotenv": "5.1.*",
        "symfony/flex": "^1.3.1",
        "symfony/framework-bundle": "5.1.*",
        "symfony/yaml": "5.1.*"
    },
    "require-dev": {
    },
    "config": {
        "optimize-autoloader": true,
        "preferred-install": {
            "*": "dist"
        },
        "sort-packages": true
    },
    "autoload": {
        "psr-4": {
            "App\\": "src/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "App\\Tests\\": "tests/"
        }
    },
    "replace": {
        "paragonie/random_compat": "2.*",
        "symfony/polyfill-ctype": "*",
        "symfony/polyfill-iconv": "*",
        "symfony/polyfill-php72": "*",
        "symfony/polyfill-php71": "*",
        "symfony/polyfill-php70": "*",
        "symfony/polyfill-php56": "*"
    },
    "scripts": {
        "auto-scripts": {
            "cache:clear": "symfony-cmd",
            "assets:install %PUBLIC_DIR%": "symfony-cmd"
        },
        "post-install-cmd": [
            "@auto-scripts"
        ],
        "post-update-cmd": [
            "@auto-scripts"
        ]
    },
    "conflict": {
        "symfony/symfony": "*"
    },
    "extra": {
        "symfony": {
            "allow-contrib": false,
            "require": "5.1.*"
        }
    }
}`
* to run secure-in-one on swoole as **HTTP2-Server** see https://www.swoole.co.uk/docs/modules/swoole-http-server-doc
* to run secure-in-one as a **lame duck service** just deploy it to some domain
* Dependencies: see composer.json
* Database configuration: not needed


### Contribution guidelines ###

* **Repo**: `git clone https://Jochen_Treaction@bitbucket.org/Jochen_Treaction/secure-in-one.git`
* Writing tests: n.a.
* Code review: n.a.
* Other guidelines: n.a.

### Who do I talk to? ###

* Repo owner or admin
* team
